#' Generate string with left character pad and right padded numbering to generate e.g. NM0001, NM0002
#'
#' @param string left pad string
#' @param nums vector with numbers
#' @param width character length of number with padding of 0s
#'
#' @return
#' @export
#'
#' @examples
str_pad_textnum <- function(string, nums = 1, width = 6) {
  paste0(string, str_pad(nums, width = width, pad = "0"))
}
