#' Get FreezerWorks list views
#'
#' @param rcon freezerworks connection setup with freezerworks_connection()
#'
#' @return tibble
#' @export
#'
#' @examples
get_listviews <- function(rcon) {
  require(httr)
  require(jsonlite)
  require(tidyverse)

  json <- GET(
    paste0("http://", rcon$ip, "/api/v1/listviews?limit=0"),
    add_headers(
      "Authorization" = paste("Basic", rcon$auth_key),
      "Accept" = "application/vnd.siren+json"
    )
  ) %>%
    content(as = "text", encoding = "UTF-8") %>%
    fromJSON()

  as_tibble(json$entities$properties)
}
