#' Get the next series alphanum e.g. NM00001 -> NM00002
#'
#' @param current_aliquot_tbl_column vector of alphanum characters
#' @param num_to_add numbers to add e.g. 2 for NM00003, NM00004
#'
#' @return
#' @export
#'
#' @examples
get_next_alphanum <- function(current_aliquot_tbl_column, num_to_add = 1) {
  pad <- get_pad(current_aliquot_tbl_column)
  num <- get_num(current_aliquot_tbl_column)
  next_num <- max(as.numeric(num)) + 1
  next_nums <- as.character(seq(next_num, next_num + num_to_add - 1))

  str_pad_textnum(pad[1], next_nums)
}
